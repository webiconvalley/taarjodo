<main id="tt-pageContent">
         <div class="container-indent no-margin mainSlider-wrapper">
            <div class="loading-content">
               <div class="loading-dots"><img src="<?php echo base_url(); ?>assets/images/bolt.gif" alt=""></div>
            </div>
            <div id="js-mainSlider" class="mainSlider">
               <div class="slide">
                  <div class="img--holder" data-bgslide="<?php echo base_url(); ?>assets/images/mainslide-01.jpg"></div>
                  <div class="slide-content">
                     <div class="container text-center js-rotation" data-animation="fadeInUpSm" data-animation-delay="0s">
                        <div class="tt-title-01">Keeping You Wired</div>
                        <div class="tt-title-02">We’re the Current<br>Specialist!</div>
                     </div>
                  </div>
               </div>
               <div class="slide">
                  <div class="img--holder" data-bgslide="<?php echo base_url(); ?>assets/images/mainslide-02.jpg"></div>
                  <div class="slide-content">
                     <div class="container text-center js-rotation" data-animation="fadeInUpSm" data-animation-delay="0s">
                        <div class="tt-title-01">Making Our Clients Happier</div>
                        <div class="tt-title-02">Best Services<br>for Your Family</div>
                     </div>
                  </div>
               </div>
               <div class="slide">
                  <div class="img--holder" data-bgslide="<?php echo base_url(); ?>assets/images/mainslide-03.jpg"></div>
                  <div class="slide-content">
                     <div class="container text-center js-rotation" data-animation="fadeInUpSm" data-animation-delay="0s">
                        <div class="tt-title-01">We Can Light Everything</div>
                        <div class="tt-title-02">Nothing is Impossible<br>for Us</div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="container order-form-wrapper container-lg-fluid container-lg__no-gutters">
            <div class="order-form">
               <div class="order-form__title" id="js-toggle-orderform"><i class="tt-arrow down"></i> Request Service Today</div>
               <div class="order-form__content form-order">
                  <form id="orderform" method="post" novalidate="novalidate" action="#">
                     <div class="form-group"><input type="text" name="name" class="form-control" placeholder="Your name"></div>
                     <div class="form-group"><input type="text" name="email" class="form-control" placeholder="Your e-mail"></div>
                     <div class="form-group"><input type="text" name="phonenumber" class="form-control" placeholder="Your phone"></div>
                     <div class="form-group">
                        <span class="icon icon-747993"></span> <input name="date" class="datepicker-1 form-control" autocomplete="off" placeholder="Date" type="text">
                        <div class="form-group__icon icon-calendar"></div>
                     </div>
                     <div class="form-group"><button class="tt-btn btn__color01" type="submit"><span class="icon-lightning"></span>Get Service</button></div>
                  </form>
               </div>
            </div>
         </div>
         <div class="section-indent">
            <div class="container container-lg-fluid">
               <div class="layout01 layout01__img-more">
                  <div class="layout01__img">
                     <div class="tt-img-main"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAqAAAAIoAQMAAACMEcszAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAAERJREFUeNrtwQENAAAAwiD7p7bHBwwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIC0A7dIAAFubGEWAAAAAElFTkSuQmCC" data-src="<?php echo base_url(); ?>assets/images/layout01-img01.jpg" class="lazyload" alt=""></div>
                     <div class="tt-img-more left-bottom"><img src="data:image/gif;base64,R0lGODdhAQABAPAAAMPDwwAAACwAAAAAAQABAAACAkQBADs=" data-src="<?php echo base_url(); ?>assets/images/layout01-img02.jpg" class="lazyload" alt=""></div>
                  </div>
                  <div class="layout01__content">
                     <div class="layout01__content-wrapper">
                        <div class="section-title text-left">
                           <div class="section-title__01">About Us</div>
                           <div class="section-title__02">Outstanding Residential &amp; Commercial Services</div>
                        </div>
                        <p>All of our services are backed by our 100% satisfaction guarantee. Our electricians can install anything from new security lighting for your outdoors to a whole home generator that will keep your appliances working during a power outage.</p>
                        <ul class="tt-list01 tt-list-top">
                           <li>Full-service electrical layout, design</li>
                           <li>Wiring and installation/upgrades</li>
                           <li>Emergency power solutions (generators)</li>
                           <li>Virtually any electrical needs you have – just ask!</li>
                        </ul>
                        <div class="tt-data-info">
                           <div class="tt-item">
                              <div class="personal-box">
                                 <div class="personal-box__img"><img src="data:image/gif;base64,R0lGODdhAQABAPAAAMPDwwAAACwAAAAAAQABAAACAkQBADs=" data-src="<?php echo base_url(); ?>assets/images/img-02.jpg" class="lazyload" alt=""></div>
                                 <div class="personal-box__content">
                                    <div class="personal-box__title">Mark Smith</div>
                                    Your own electrician
                                 </div>
                              </div>
                           </div>
                           <div class="tt-item"><img src="data:image/gif;base64,R0lGODdhAQABAPAAAMPDwwAAACwAAAAAAQABAAACAkQBADs=" data-src="<?php echo base_url(); ?>assets/images/img-01.png" class="lazyload" alt=""></div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="section-indent">
            <div class="tt-slideinfo-wrapper slick-type01">
               <div class="tt-slideinfo">
                  <div class="tt-item__bg">
                     <div data-bg="<?php echo base_url(); ?>assets/images/slideinfo-01.jpg" class="lazyload tt-item__bg-img"></div>
                     <div class="tt-item__bg-top"></div>
                  </div>
                  <div class="tt-item__content">
                     <div class="tt-item__title"><span class="tt-icon"><img src="data:image/gif;base64,R0lGODdhAQABAPAAAMPDwwAAACwAAAAAAQABAAACAkQBADs=" class="lazyload" data-src="<?php echo base_url(); ?>assets/images/slideinfo-marker.png" alt=""></span><span class="tt-text">Commercial</span></div>
                     <div class="tt-item__description">We offer the highest level of responsiveness and reliability, including on-line job management and reporting. Our highly experienced contractors across the nation ensure that your premises are always maintained and compliant.</div>
                     <div class="tt-item__btn"><a href="#">+</a></div>
                  </div>
               </div>
               <div class="tt-slideinfo">
                  <div class="tt-item__bg">
                     <div data-bg="<?php echo base_url(); ?>assets/images/slideinfo-02.jpg" class="lazyload tt-item__bg-img"></div>
                     <div class="tt-item__bg-top"></div>
                  </div>
                  <div class="tt-item__content">
                     <div class="tt-item__title"><span class="tt-icon"><img src="data:image/gif;base64,R0lGODdhAQABAPAAAMPDwwAAACwAAAAAAQABAAACAkQBADs=" class="lazyload" data-src="<?php echo base_url(); ?>assets/images/slideinfo-marker.png" alt=""></span><span class="tt-text">Industrial</span></div>
                     <div class="tt-item__description">We offer the highest level of responsiveness and reliability, including on-line job management and reporting. Our highly experienced contractors across the nation ensure that your premises are always maintained and compliant.</div>
                     <div class="tt-item__btn"><a href="#">+</a></div>
                  </div>
               </div>
               <div class="tt-slideinfo">
                  <div class="tt-item__bg">
                     <div data-bg="<?php echo base_url(); ?>assets/images/slideinfo-03.jpg" class="lazyload tt-item__bg-img"></div>
                     <div class="tt-item__bg-top"></div>
                  </div>
                  <div class="tt-item__content">
                     <div class="tt-item__title"><span class="tt-icon"><img src="data:image/gif;base64,R0lGODdhAQABAPAAAMPDwwAAACwAAAAAAQABAAACAkQBADs=" class="lazyload" data-src="<?php echo base_url(); ?>assets/images/slideinfo-marker.png" alt=""></span><span class="tt-text">Residential</span></div>
                     <div class="tt-item__description">We offer the highest level of responsiveness and reliability, including on-line job management and reporting. Our highly experienced contractors across the nation ensure that your premises are always maintained and compliant.</div>
                     <div class="tt-item__btn"><a href="#">+</a></div>
                  </div>
               </div>
            </div>
         </div>
         <div class="section-indent">
            <div class="container container-lg-fluid container__p-r">
               <div class="section-marker section-marker_b-l"><img class="lazyload" src="data:image/gif;base64,R0lGODdhAQABAPAAAMPDwwAAACwAAAAAAQABAAACAkQBADs=" data-src="<?php echo base_url(); ?>assets/images/bg_marker02.png" alt=""></div>
               <div class="section-title max-width-01 section-title_indent-02">
                  <div class="section-title__01">24/7 Electrician Services – Safe and Efficient</div>
                  <div class="section-title__02">We are a Full Service Electrical Contractor</div>
               </div>
               <div class="tt-box02_wrapper row slick-type01 slick-slider slick-dotted" data-slick='{
                  "slidesToShow": 3,
                  "autoplaySpeed": 4500,
                  "slidesToScroll": 2,
                  "responsive": [
                  {
                  "breakpoint": 750,
                  "settings": {
                  "slidesToShow": 2
                  }
                  },
                  {
                  "breakpoint": 546,
                  "settings": {
                  "slidesToShow": 1,
                  "slidesToScroll": 1
                  }
                  }
                  ]
                  }'>
                  <div class="col-md-4">
                     <div class="tt-box02">
                        <a href="services-item.html" class="tt-box02__img icon-light">
                           <div class="tt-bg-dark"><img data-src="<?php echo base_url(); ?>assets/images/box02-img01.jpg" src="data:image/gif;base64,R0lGODdhAQABAPAAAMPDwwAAACwAAAAAAQABAAACAkQBADs=" class="tt-img-main lazyload" alt=""></div>
                           <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAI4AAADdAQMAAABXKdgSAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAABtJREFUWMPtwTEBAAAAwiD7p7bETmAAAAAAjQMQZwAByGriPgAAAABJRU5ErkJggg==" data-src="<?php echo base_url(); ?>assets/images/mask-img01.png" class="tt-img-mask lazyload" alt="">
                        </a>
                        <h6 class="tt-box02__title"><a href="#">Electrical Service</a></h6>
                        <p>We provide complete electrical design and installation services.</p>
                        <div class="tt-row-btn"><a href="services-item.html" class="tt-link">More info<span class="icon-arrowhead-pointing-to-the-right-1"></span></a></div>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="tt-box02">
                        <a href="services-item.html" class="tt-box02__img icon-tool">
                           <div class="tt-bg-dark"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAI4AAADdAQMAAABXKdgSAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAABtJREFUWMPtwTEBAAAAwiD7p7bETmAAAAAAjQMQZwAByGriPgAAAABJRU5ErkJggg==" data-src="<?php echo base_url(); ?>assets/images/box02-img02.jpg" class="tt-img-main lazyload" alt=""></div>
                           <img data-src="<?php echo base_url(); ?>assets/images/mask-img01.png" src="data:image/gif;base64,R0lGODdhAQABAPAAAMPDwwAAACwAAAAAAQABAAACAkQBADs=" class="tt-img-mask lazyload" alt="">
                        </a>
                        <h6 class="tt-box02__title"><a href="#">Heating Service</a></h6>
                        <p>We offer energy efficient electric heat products and installations.</p>
                        <div class="tt-row-btn"><a href="services-item.html" class="tt-link">More info<span class="icon-arrowhead-pointing-to-the-right-1"></span></a></div>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="tt-box02">
                        <a href="services-item.html" class="tt-box02__img icon-air-conditioner">
                           <div class="tt-bg-dark"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAI4AAADdAQMAAABXKdgSAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAABtJREFUWMPtwTEBAAAAwiD7p7bETmAAAAAAjQMQZwAByGriPgAAAABJRU5ErkJggg==" data-src="<?php echo base_url(); ?>assets/images/box02-img03.jpg" class="tt-img-main lazyload" alt=""></div>
                           <img data-src="<?php echo base_url(); ?>assets/images/mask-img01.png" src="data:image/gif;base64,R0lGODdhAQABAPAAAMPDwwAAACwAAAAAAQABAAACAkQBADs=" class="tt-img-mask lazyload" alt="">
                        </a>
                        <h6 class="tt-box02__title"><a href="#">Air Conditioning</a></h6>
                        <p>Our installation services ensure that you get the right air conditioner.</p>
                        <div class="tt-row-btn"><a href="services-item.html" class="tt-link">More info<span class="icon-arrowhead-pointing-to-the-right-1"></span></a></div>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="tt-box02">
                        <a href="services-item.html" class="tt-box02__img icon-security-camera">
                           <div class="tt-bg-dark"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAI4AAADdAQMAAABXKdgSAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAABtJREFUWMPtwTEBAAAAwiD7p7bETmAAAAAAjQMQZwAByGriPgAAAABJRU5ErkJggg==" data-src="<?php echo base_url(); ?>assets/images/box02-img04.jpg" class="tt-img-main lazyload" alt=""></div>
                           <img data-src="<?php echo base_url(); ?>assets/images/mask-img01.png" src="data:image/gif;base64,R0lGODdhAQABAPAAAMPDwwAAACwAAAAAAQABAAACAkQBADs=" class="tt-img-mask lazyload" alt="">
                        </a>
                        <h6 class="tt-box02__title"><a href="#">Security Systems</a></h6>
                        <p>You can view events over a monitor in our home.</p>
                        <div class="tt-row-btn"><a href="services-item.html" class="tt-link">More info<span class="icon-arrowhead-pointing-to-the-right-1"></span></a></div>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="tt-box02">
                        <a href="services-item.html" class="tt-box02__img icon-computer">
                           <div class="tt-bg-dark"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAI4AAADdAQMAAABXKdgSAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAABtJREFUWMPtwTEBAAAAwiD7p7bETmAAAAAAjQMQZwAByGriPgAAAABJRU5ErkJggg==" data-src="<?php echo base_url(); ?>assets/images/box02-img05.jpg" class="tt-img-main lazyload" alt=""></div>
                           <img data-src="<?php echo base_url(); ?>assets/images/mask-img01.png" src="data:image/gif;base64,R0lGODdhAQABAPAAAMPDwwAAACwAAAAAAQABAAACAkQBADs=" class="tt-img-mask lazyload" alt="">
                        </a>
                        <h6 class="tt-box02__title"><a href="#">Panels Changes</a></h6>
                        <p>Electrical panels are the heart of your electrical system.</p>
                        <div class="tt-row-btn"><a href="services-item.html" class="tt-link">More info<span class="icon-arrowhead-pointing-to-the-right-1"></span></a></div>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="tt-box02">
                        <a href="services-item.html" class="tt-box02__img icon-screwdriver-and-wrench-crossed">
                           <div class="tt-bg-dark"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAI4AAADdAQMAAABXKdgSAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAABtJREFUWMPtwTEBAAAAwiD7p7bETmAAAAAAjQMQZwAByGriPgAAAABJRU5ErkJggg==" data-src="<?php echo base_url(); ?>assets/images/box02-img06.jpg" class="tt-img-main lazyload" alt=""></div>
                           <img data-src="<?php echo base_url(); ?>assets/images/mask-img01.png" src="data:image/gif;base64,R0lGODdhAQABAPAAAMPDwwAAACwAAAAAAQABAAACAkQBADs=" class="tt-img-mask lazyload" alt="">
                        </a>
                        <h6 class="tt-box02__title"><a href="#">Trouble Shooting</a></h6>
                        <p>We think before we start working to save you money.</p>
                        <div class="tt-row-btn"><a href="services-item.html" class="tt-link">More info<span class="icon-arrowhead-pointing-to-the-right-1"></span></a></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="section-indent">
            <div class="tt-box01 js-init-bg lazyload" data-bg="<?php echo base_url(); ?>assets/images/box01-bg-desktop.jpg">
               <div class="container">
                  <div class="tt-box01__holder">
                     <div class="tt-box01__description">
                        <h4 class="tt-box01__title">Do you <span class="tt-base-color">Need Help</span><br>With Electrical<br>Maintenance?</h4>
                        <p>Our electrical repair and service options are proudly offered to clients. Give us a call today to schedule a free service estimate!</p>
                        <div class="tt-row-btn"><a class="tt-btn btn__color01" href="tel:1(800)7654321"><span class="icon-telephone"></span>Give Us a Call</a> <a class="tt-btn btn__color02" data-toggle="modal" data-target="#modalMakeAppointment" href="#"><span class="icon-lightning"></span>Free Estimate</a></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="section-indent">
            <div class="container container-md-fluid">
               <div class="section-title max-width-01">
                  <div class="section-title__01"><a href="https://www.instagram.com/" target="_blank">@electricians</a></div>
                  <div class="section-title__02">Our Projects</div>
               </div>
               <div id="filter-nav">
                  <ul>
                     <li class="active"><a class="gallery-navitem" href="all.html">All</a></li>
                     <li><a class="gallery-navitem" href="residences.html">Residences</a></li>
                     <li><a class="gallery-navitem" href="industrial_objects.html">Industrial_Objects</a></li>
                     <li><a class="gallery-navitem" href="offices.html">Offices</a></li>
                     <li><a class="gallery-navitem" href="retail_objects.html">Retail_Objects</a></li>
                  </ul>
               </div>
               <div id="filter-layout" class="row justify-content-center gallery-innerlayout-wrapper js-wrapper-gallery">
                  <div class="col-4 col-md-3 col-custom-item5 residences show all">
                     <a href="<?php echo base_url(); ?>assets/images/gallery01-01.jpg" class="tt-gallery">
                        <div class="gallery__icon"></div>
                        <img class="lazyload" src="data:image/gif;base64,R0lGODdhAQABAPAAAMPDwwAAACwAAAAAAQABAAACAkQBADs=" data-src="<?php echo base_url(); ?>assets/images/gallery01-01.jpg" alt="">
                     </a>
                  </div>
                  <div class="col-4 col-md-3 col-custom-item5 residences show all">
                     <a href="<?php echo base_url(); ?>assets/images/gallery01-02.jpg" class="tt-gallery">
                        <div class="gallery__icon"></div>
                        <img class="lazyload" src="data:image/gif;base64,R0lGODdhAQABAPAAAMPDwwAAACwAAAAAAQABAAACAkQBADs=" data-src="<?php echo base_url(); ?>assets/images/gallery01-02.jpg" alt="">
                     </a>
                  </div>
                  <div class="col-4 col-md-3 col-custom-item5 residences show all">
                     <a href="<?php echo base_url(); ?>assets/images/gallery01-03.jpg" class="tt-gallery">
                        <div class="gallery__icon"></div>
                        <img class="lazyload" src="data:image/gif;base64,R0lGODdhAQABAPAAAMPDwwAAACwAAAAAAQABAAACAkQBADs=" data-src="<?php echo base_url(); ?>assets/images/gallery01-03.jpg" alt="">
                     </a>
                  </div>
                  <div class="col-4 col-md-3 col-custom-item5 industrial_objects show all">
                     <a href="<?php echo base_url(); ?>assets/images/gallery01-04.jpg" class="tt-gallery">
                        <div class="gallery__icon"></div>
                        <img class="lazyload" src="data:image/gif;base64,R0lGODdhAQABAPAAAMPDwwAAACwAAAAAAQABAAACAkQBADs=" data-src="<?php echo base_url(); ?>assets/images/gallery01-04.jpg" alt="">
                     </a>
                  </div>
                  <div class="col-4 col-md-3 col-custom-item5 offices show all">
                     <a href="<?php echo base_url(); ?>assets/images/gallery01-05.jpg" class="tt-gallery">
                        <div class="gallery__icon"></div>
                        <img class="lazyload" src="data:image/gif;base64,R0lGODdhAQABAPAAAMPDwwAAACwAAAAAAQABAAACAkQBADs=" data-src="<?php echo base_url(); ?>assets/images/gallery01-05.jpg" alt="">
                     </a>
                  </div>
                  <div class="col-4 col-md-3 col-custom-item5 industrial_objects show all">
                     <a href="<?php echo base_url(); ?>assets/images/gallery01-06.jpg" class="tt-gallery">
                        <div class="gallery__icon"></div>
                        <img class="lazyload" src="data:image/gif;base64,R0lGODdhAQABAPAAAMPDwwAAACwAAAAAAQABAAACAkQBADs=" data-src="<?php echo base_url(); ?>assets/images/gallery01-06.jpg" alt="">
                     </a>
                  </div>
                  <div class="col-4 col-md-3 col-custom-item5 retail_objects show all">
                     <a href="<?php echo base_url(); ?>assets/images/gallery01-07.jpg" class="tt-gallery">
                        <div class="gallery__icon"></div>
                        <img class="lazyload" src="data:image/gif;base64,R0lGODdhAQABAPAAAMPDwwAAACwAAAAAAQABAAACAkQBADs=" data-src="<?php echo base_url(); ?>assets/images/gallery01-07.jpg" alt="">
                     </a>
                  </div>
                  <div class="col-4 col-md-3 col-custom-item5 offices show all">
                     <a href="<?php echo base_url(); ?>assets/images/gallery01-08.jpg" class="tt-gallery">
                        <div class="gallery__icon"></div>
                        <img class="lazyload" src="data:image/gif;base64,R0lGODdhAQABAPAAAMPDwwAAACwAAAAAAQABAAACAkQBADs=" data-src="<?php echo base_url(); ?>assets/images/gallery01-08.jpg" alt="">
                     </a>
                  </div>
                  <div class="col-4 col-md-3 col-custom-item5 retail_objects show all">
                     <a href="<?php echo base_url(); ?>assets/images/gallery01-09.jpg" class="tt-gallery">
                        <div class="gallery__icon"></div>
                        <img class="lazyload" src="data:image/gif;base64,R0lGODdhAQABAPAAAMPDwwAAACwAAAAAAQABAAACAkQBADs=" data-src="<?php echo base_url(); ?>assets/images/gallery01-09.jpg" alt="">
                     </a>
                  </div>
                  <div class="col-4 col-md-3 col-custom-item5 retail_objects show all">
                     <a href="<?php echo base_url(); ?>assets/images/gallery01-10.jpg" class="tt-gallery">
                        <div class="gallery__icon"></div>
                        <img class="lazyload" src="data:image/gif;base64,R0lGODdhAQABAPAAAMPDwwAAACwAAAAAAQABAAACAkQBADs=" data-src="<?php echo base_url(); ?>assets/images/gallery01-10.jpg" alt="">
                     </a>
                  </div>
                  <div class="col-12 text-center tt-top-more" id="js-more-include" data-include="ajax-content/more-gallery-innerlayout.php"><a href="#" class="tt-link tt-link__lg">View all projects<span class="icon-arrowhead-pointing-to-the-right-1"></span></a></div>
               </div>
            </div>
         </div>
         <div class="section-indent">
            <div class="section__wrapper">
               <div class="container container-md-fluid">
                  <div class="tt-info-value row">
                     <div class="tt-col-title col-md-4">
                        <div class="tt-title">
                           <img class="bg-marker lazyload" src="data:image/gif;base64,R0lGODdhAQABAPAAAMPDwwAAACwAAAAAAQABAAACAkQBADs=" data-src="<?php echo base_url(); ?>assets/images/bg_marker.png" alt="">
                           <div class="tt-title__01">Our Statistics</div>
                           <div class="tt-title__02">Some Important Facts</div>
                        </div>
                     </div>
                     <div class="col-auto ml-auto">
                        <div class="tt-item">
                           <div class="tt-value">5000+</div>
                           Residential Projects
                        </div>
                     </div>
                     <div class="col-auto">
                        <div class="tt-item">
                           <div class="tt-value">1500+</div>
                           Commercial Projects
                        </div>
                     </div>
                     <div class="col-auto">
                        <div class="tt-item">
                           <div class="tt-value">1000+</div>
                           Industrial Projects
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="tt-box03 tt-box03__extraindent">
            <div class="container container-md-fluid">
               <div class="row no-gutters">
                  <div class="col-md-7">
                     <div class="tt-box03__content">
                        <div class="slick-type01 slick-dots-left" data-slick='{
                           "slidesToShow": 1,
                           "slidesToScroll": 1,
                           "autoplaySpeed": 4500
                           }'>
                           <div class="item">
                              <div class="item__row">
                                 <div class="tt-item__img"><img class="lazyload" src="data:image/gif;base64,R0lGODdhAQABAPAAAMPDwwAAACwAAAAAAQABAAACAkQBADs=" data-src="<?php echo base_url(); ?>assets/images/box03_img02.jpg" alt=""></div>
                                 <div class="tt-item__title">
                                    <div class="section-title text-left">
                                       <div class="section-title__01">What Our Clients Say</div>
                                       <div class="section-title__02">Professional, Reliable & Cost Effective</div>
                                    </div>
                                 </div>
                              </div>
                              <div class="tt-item__content">
                                 <blockquote>We've been using your company and from the very beginning found him and his team to be extremely professional and knowledgeable. We wouldn't have any hesitation in recommending their services. <cite>- Teresa and Kevin K.</cite></blockquote>
                              </div>
                           </div>
                           <div class="item">
                              <div class="item__row">
                                 <div class="tt-item__img"><img class="lazyload" src="data:image/gif;base64,R0lGODdhAQABAPAAAMPDwwAAACwAAAAAAQABAAACAkQBADs=" data-src="<?php echo base_url(); ?>assets/images/box03_img02.jpg" alt=""></div>
                                 <div class="tt-item__title">
                                    <div class="section-title text-left">
                                       <div class="section-title__01">What Our Clients Say</div>
                                       <div class="section-title__02">Professional, Reliable & Cost Effective</div>
                                    </div>
                                 </div>
                              </div>
                              <div class="tt-item__content">
                                 <blockquote>We've been using your company and from the very beginning found him and his team to be extremely professional and knowledgeable. We wouldn't have any hesitation in recommending their services. <cite>- Teresa and Kevin K.</cite></blockquote>
                              </div>
                           </div>
                           <div class="item">
                              <div class="item__row">
                                 <div class="tt-item__img"><img class="lazyload" src="data:image/gif;base64,R0lGODdhAQABAPAAAMPDwwAAACwAAAAAAQABAAACAkQBADs=" data-src="<?php echo base_url(); ?>assets/images/box03_img02.jpg" alt=""></div>
                                 <div class="tt-item__title">
                                    <div class="section-title text-left">
                                       <div class="section-title__01">What Our Clients Say</div>
                                       <div class="section-title__02">Professional, Reliable & Cost Effective</div>
                                    </div>
                                 </div>
                              </div>
                              <div class="tt-item__content">
                                 <blockquote>We've been using your company and from the very beginning found him and his team to be extremely professional and knowledgeable. We wouldn't have any hesitation in recommending their services. <cite>- Teresa and Kevin K.</cite></blockquote>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="tt-box03__img tt-visible-mobile lazyload" data-bg="<?php echo base_url(); ?>assets/images/box03_img01.jpg"></div>
                     <div class="tt-box03__extra">
                        <div class="tt-title">Emergency Service</div>
                        <p>If this is an emergency outside of normal business hours, call us</p>
                        <address><a href="tel:1(800)7654321"><i class="icon-telephone"></i> 1 (800) 765-43-21</a></address>
                     </div>
                  </div>
                  <div class="tt-box03__img tt-visible-desktop lazyload" data-bg="<?php echo base_url(); ?>assets/images/box03_img01.jpg"></div>
               </div>
            </div>
         </div>
         <div class="section-indent">
            <div class="container">
               <div class="section-title max-width-01">
                  <div class="section-title__01">Save on the Service You Need</div>
                  <div class="section-title__02">Maintenance Plans</div>
                  <div class="section-title__03">With an electrical maintenance plan, you won’t find yourself in a panic wondering who to call when you’re having problems with your electrical system.</div>
               </div>
               <div class="tt-block-marker">
                  <img class="bg-marker01 block-marker__obj lazyload" src="data:image/gif;base64,R0lGODdhAQABAPAAAMPDwwAAACwAAAAAAQABAAACAkQBADs=" data-src="<?php echo base_url(); ?>assets/images/bg_marker02.png" alt="">
                  <div class="tt-layout02-wrapper slick-type01 row" data-slick='{
                     "slidesToShow": 3,
                     "slidesToScroll": 2,
                     "autoplaySpeed": 5000,
                     "responsive": [
                     {
                     "breakpoint": 1230,
                     "settings": {
                     "slidesToShow": 2,
                     "slidesToScroll": 2
                     }
                     },
                     {
                     "breakpoint": 767,
                     "settings": {
                     "slidesToShow": 1,
                     "slidesToScroll": 1
                     }
                     }
                     ]
                     }'>
                     <div class="item col-md-4">
                        <div class="tt-layout02">
                           <div class="tt-layout02__icon"><span class="icon-867257"></span></div>
                           <div class="tt-layout02__title">Commercial Service</div>
                           <ul class="tt-layout02__list">
                              <li>Indoor/outdoor Lighting Installation</li>
                              <li>Appliance &amp; Fixture Installation</li>
                              <li>Annual Electrical Inspection</li>
                              <li>Ceiling Fan Installation</li>
                              <li>New &amp; Replacement Wiring</li>
                              <li>Surge Protection Maintenance</li>
                              <li>24-hour Response</li>
                           </ul>
                           <hr class="tt-layout02__hr">
                           <div class="tt-layout02__price">$89.00</div>
                           <div class="tt-layout02__link"><a href="#" data-toggle="modal" data-target="#modalMakeAppointment" class="tt-link">Order now<span class="icon-arrowhead-pointing-to-the-right-1"></span></a></div>
                        </div>
                     </div>
                     <div class="item col-md-4">
                        <div class="tt-layout02">
                           <div class="tt-layout02__icon"><span class="icon-621023"></span></div>
                           <div class="tt-layout02__title">Industrial Service</div>
                           <ul class="tt-layout02__list">
                              <li>Annual A/C inspection</li>
                              <li>Annual electrical inspection</li>
                              <li>Install new double power</li>
                              <li>Retrofits and Upgrades</li>
                              <li>Install double power outside</li>
                              <li>Switchboard Upgrade</li>
                              <li>Critical Installations</li>
                           </ul>
                           <hr class="tt-layout02__hr">
                           <div class="tt-layout02__price">$130.00</div>
                           <div class="tt-layout02__link"><a href="#" data-toggle="modal" data-target="#modalMakeAppointment" class="tt-link">Order now<span class="icon-arrowhead-pointing-to-the-right-1"></span></a></div>
                        </div>
                     </div>
                     <div class="item col-md-4">
                        <div class="tt-layout02">
                           <div class="tt-layout02__icon"><span class="icon-plug-1"></span></div>
                           <div class="tt-layout02__title">Residential Service</div>
                           <ul class="tt-layout02__list">
                              <li>Annual A/C inspection</li>
                              <li>Annual electrical inspection</li>
                              <li>Design-Build Services</li>
                              <li>Supply and install Sensor light</li>
                              <li>Lighting Fixtures</li>
                              <li>Replace hot plates</li>
                              <li>Switchboard Upgrade</li>
                           </ul>
                           <hr class="tt-layout02__hr">
                           <div class="tt-layout02__price">$12.00</div>
                           <div class="tt-layout02__link"><a href="#" data-toggle="modal" data-target="#modalMakeAppointment" class="tt-link">Order now<span class="icon-arrowhead-pointing-to-the-right-1"></span></a></div>
                        </div>
                     </div>
                     <div class="item col-md-4">
                        <div class="tt-layout02">
                           <div class="tt-layout02__icon"><span class="icon-867257"></span></div>
                           <div class="tt-layout02__title">Commercial Service</div>
                           <ul class="tt-layout02__list">
                              <li>Indoor/outdoor Lighting Installation</li>
                              <li>Appliance &amp; Fixture Installation</li>
                              <li>Annual Electrical Inspection</li>
                              <li>Ceiling Fan Installation</li>
                              <li>New &amp; Replacement Wiring</li>
                              <li>Surge Protection Maintenance</li>
                              <li>24-hour Response</li>
                           </ul>
                           <hr class="tt-layout02__hr">
                           <div class="tt-layout02__price">$89.00</div>
                           <div class="tt-layout02__link"><a href="#" data-toggle="modal" data-target="#modalMakeAppointment" class="tt-link">Order now<span class="icon-arrowhead-pointing-to-the-right-1"></span></a></div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="section-indent">
            <div class="tt-box01 lazyload" data-bg="<?php echo base_url(); ?>assets/images/box01-bg02-desktop.jpg">
               <div class="container">
                  <div class="tt-box01__holder">
                     <div class="tt-box01__video"><a href="https://www.youtube.com/watch?v=_sI_Ps7JSEk" class="tt-video js-video-popup"><i class="icon-arrowhead-pointing-to-the-right-1"></i></a></div>
                     <div class="tt-box01__description">
                        <h4 class="tt-box01__title">Your <span class="tt-base-color">Best Option</span><br>in Electrical<br>Contractors 24/7</h4>
                        <p>Our experienced electricians are highly trained in all aspects of electrical service, from office lighting and security systems to emergency repair.</p>
                        <div class="tt-row-btn"><a class="tt-btn btn__color01" data-toggle="modal" data-target="#modalMakeAppointment" href="#"><span class="icon-lightning"></span>Explore services</a></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="section-indent">
            <div class="container container-md-fluid">
               <div class="row">
                  <div class="col-sm-6 col-lg-4">
                     <div class="section-title text-left section-title_indent-01">
                        <div class="section-title__01">Latest News</div>
                        <h4 class="section-title__02">News &amp; Update</h4>
                        <img class="bg-marker01 lazyload" src="data:image/gif;base64,R0lGODdhAQABAPAAAMPDwwAAACwAAAAAAQABAAACAkQBADs=" data-src="<?php echo base_url(); ?>assets/images/bg_marker02.png" alt="">
                     </div>
                     <div class="tt-news-list">
                        <div class="tt-item">
                           <div class="tt-item_data">2 May, 2020</div>
                           <h4 class="tt-item__title"><a href="blog-item.html">The Best Small Kitchen Appliances</a></h4>
                           <p>If you notice an electrical outlet smoking, it’s a serious issue that requires immediate action.</p>
                        </div>
                        <div class="tt-item">
                           <div class="tt-item_data">5 May, 2020</div>
                           <h4 class="tt-item__title"><a href="blog-item.html">How to Fix a Smoking Electrical Outlet</a></h4>
                           <p>Although the kitchen has been referred to as the heart of the home, it can also be...</p>
                        </div>
                     </div>
                  </div>
                  <div class="divider d-block d-sm-none"></div>
                  <div class="col-sm-6 col-lg-8">
                     <div class="tt-news-col row js-init-carusel-04 slick-type01">
                        <div class="col-md-12 col-lg-6">
                           <div class="tt-news-obj">
                              <div class="tt-news-obj__img"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAXIAAAEYAQMAAAB7he92AAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAACRJREFUaN7twYEAAAAAw6D7U59gBtUAAAAAAAAAAAAAAAAAwgE0gAABfVm7PgAAAABJRU5ErkJggg==" class="lazyload" data-src="<?php echo base_url(); ?>assets/images/news-obj-img01.jpg" alt=""></div>
                              <div class="tt-news-obj__wrapper">
                                 <div class="tt-news-obj__data">6 May, 2020</div>
                                 <div class="tt-news-obj__title"><a href="blog-item.html">How to Get Electricity to a Kitchen Island</a></div>
                                 <p>Do you need an expert solution to get electricity to a kitchen island? You may be feeling overwhelmed...</p>
                                 <div class="row-btn"><a href="blog-item.html" class="tt-link">Read more<span class="icon-arrowhead-pointing-to-the-right-1"></span></a></div>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6 col-lg-6">
                           <div class="tt-news-obj">
                              <div class="tt-news-obj__img"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAXIAAAEYAQMAAAB7he92AAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAACRJREFUaN7twYEAAAAAw6D7U59gBtUAAAAAAAAAAAAAAAAAwgE0gAABfVm7PgAAAABJRU5ErkJggg==" class="lazyload" data-src="<?php echo base_url(); ?>assets/images/news-obj-img02.jpg" alt=""></div>
                              <div class="tt-news-obj__wrapper">
                                 <div class="tt-news-obj__data">8 May, 2020</div>
                                 <div class="tt-news-obj__title"><a href="blog-item.html">The Best Way to Clean Light Fixtures</a></div>
                                 <p>While cleaning light fixtures may not be at the top of your to-do list, it’s one of those simple tasks...</p>
                                 <div class="row-btn"><a href="blog-item.html" class="tt-link">Read more<span class="icon-arrowhead-pointing-to-the-right-1"></span></a></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="section-indent section_hr">
            <div class="container container-md-fluid">
               <div class="tt-logo-list slick-type01 js-init-carusel-05">
                  <div class="tt-item"><a href="#"><img class="lazyload" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALQAAAAyAQMAAADP6mO0AAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAABFJREFUOMtjGAWjYBSMgiECAASwAAFkNt7mAAAAAElFTkSuQmCC" data-src="<?php echo base_url(); ?>assets/images/logo-01.png" alt=""></a></div>
                  <div class="tt-item"><a href="#"><img class="lazyload" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALQAAAAyAQMAAADP6mO0AAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAABFJREFUOMtjGAWjYBSMgiECAASwAAFkNt7mAAAAAElFTkSuQmCC" data-src="<?php echo base_url(); ?>assets/images/logo-02.png" alt=""></a></div>
                  <div class="tt-item"><a href="#"><img class="lazyload" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALQAAAAyAQMAAADP6mO0AAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAABFJREFUOMtjGAWjYBSMgiECAASwAAFkNt7mAAAAAElFTkSuQmCC" data-src="<?php echo base_url(); ?>assets/images/logo-03.png" alt=""></a></div>
                  <div class="tt-item"><a href="#"><img class="lazyload" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALQAAAAyAQMAAADP6mO0AAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAABFJREFUOMtjGAWjYBSMgiECAASwAAFkNt7mAAAAAElFTkSuQmCC" data-src="<?php echo base_url(); ?>assets/images/logo-04.png" alt=""></a></div>
                  <div class="tt-item"><a href="#"><img class="lazyload" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALQAAAAyAQMAAADP6mO0AAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAABFJREFUOMtjGAWjYBSMgiECAASwAAFkNt7mAAAAAElFTkSuQmCC" data-src="<?php echo base_url(); ?>assets/images/logo-05.png" alt=""></a></div>
                  <div class="tt-item"><a href="#"><img class="lazyload" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALQAAAAyAQMAAADP6mO0AAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAABFJREFUOMtjGAWjYBSMgiECAASwAAFkNt7mAAAAAElFTkSuQmCC" data-src="<?php echo base_url(); ?>assets/images/logo-06.png" alt=""></a></div>
               </div>
            </div>
         </div>
      </main>